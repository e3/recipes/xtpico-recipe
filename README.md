# xtpico conda recipe

Home: "https://gitlab.esss.lu.se/beam-diagnostics/bde/modules/xtpico"

Package license: BSD 3-Clause

Recipe license: BSD 3-Clause

Summary: EPICS xtpico module
